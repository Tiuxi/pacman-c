#ifndef BOARD
#define BOARD

#define MAX_CHAR_PER_LINE 30

#include <stdlib.h>
#include <stdio.h>

char **readAllFileLine(char*, int*);
void freeArray(char **array, int size);

#endif
#ifndef DRAW
#define DRAW

#define PAIR_WALL 1
#define PAIR_BASE 2
#define PAIR_PACMAN 3
#define PAIR_CAGE 4

#define PAIR_GHOST1 10 
#define PAIR_GHOST2 11
#define PAIR_GHOST3 12
#define PAIR_GHOST4 13

#include <ncurses.h>
#include <locale.h>
#include <stdbool.h>

void initColors();

void clearCell(int, int);
void drawWall(int, int);
void drawPac(int, int);
void drawPacGum(int, int);
void drawGhost(int, int, char, int);
void drawPlayer(int, int);

void drawBoard(char**, int, bool);

#endif
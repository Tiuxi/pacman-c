/*
Copyright (c) <year>, <copyright holder>
All rights reserved.

This source code is licensed under the BSD-style license found in the
LICENSE file in the root directory of this source tree.
*/
#include "movePlayer.h"

Vector* searchPlayer(char** board, int size){
    Vector* player = (Vector*)malloc(sizeof(Vector));
    player->x = -1;
    player->y = -1;
    for (int y = 0; y < size; y++){
        int x = 0;
        while (board[y][x] != '\0'){
            if (board[y][x] == 'p'){
                player->x = x;
                player->y = y;
            }
            x++;
        }
    }
    return player;
}
/**
 * 0 = nothing
 * 1 = ate pacgum
 * 2 = in a wall
 * 3 = in a ghost
*/
int movePlayer(char** board, int size, Vector* player, Vector* vector){
    switch (board[player->y + vector->y][player->x + vector->x])
    {
    case '#':
    case 'e':
        return 2;
        break;
    case '1':
    case '2':
    case '3':
    case '4':
        return 3;
        break;
    }

    // check if in a door
    if (board[player->y][player->x] == 'n'){
        board[player->y][player->x] = 'd';
    }else {
        board[player->y][player->x] = ' ';
    }
    // check eating pacgum
    bool eatPacGum = board[player->y + vector->y][player->x + vector->x] == 'o';

    // move
    player->x += vector->x;
    player->y += vector->y;

    // check shortcut
    if (player->x == 27){player->x = 0;}
    if (player->x == -1){player->x = 26;}

    // check if goes into a door
    if (board[player->y][player->x] == 'd'){
        board[player->y][player->x] = 'n';
    }else{
        board[player->y][player->x] = 'p';
    }
    
    return eatPacGum;
}
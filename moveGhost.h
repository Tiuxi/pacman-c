#ifndef MOVEGHOST
#define MOVEGHOST

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdbool.h>
#include <ncurses.h>
#include "functions.h"

#define NUMBER_OF_GHOST 4

Vector* moveAllGhost(char** board, int size, int nboucle, Vector* ghosts, Vector player);
Vector* initGhost(char** board, int size);

#endif
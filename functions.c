/*
Copyright (c) <year>, <copyright holder>
All rights reserved.

This source code is licensed under the BSD-style license found in the
LICENSE file in the root directory of this source tree.
*/
#include "functions.h"

// search for a character C in the gameboard board
Vector searchChar(char** board, int size, char c){
    Vector searched;
    searched.x = -1;
    searched.y = -1;
    for (int y = 0; y < size; y++){
        int x = 0;
        while (board[y][x] != '\0'){
            if (board[y][x] == c){
                searched.x = x;
                searched.y = y;
            }
            x++;
        }
    }
    return searched;
}

// return the distance between v1 and v2 using pythagor's theorem
double dist(Vector v1, Vector v2){
    return sqrt((v2.x-v1.x)*(v2.x-v1.x) + (v2.y-v1.y)*(v2.y-v1.y));
}

// return random number between MIN and MAX
int randRange(int min, int max){
    return (int)(min + ((float)random() / RAND_MAX * (max - min + 1)));
}

Vector* popVectorList(Vector* v, int size, int indexPop){
    for (int i = indexPop; i < size-1; i++){
        v[i] = v[i+1];
    }
    v = realloc(v, (size-1) * sizeof(Vector));
    return v;
}

// return closest Vector from v to player (p)
int closest(Vector p, Vector *v, int size){
    int index = 0;
    double minDist = dist(v[0],p);
    for (int i = 1; i<size; i++){
        double currentDist = dist(v[i],p);
        if (currentDist < minDist){
            minDist = currentDist;
            index = i;
        }
    }
    return index;
}

// return TRUE if v1 == v2
bool equalsVector(Vector v1, Vector v2){
    return v1.x == v2.x && v1.y == v2.y;
}

// return every cells next to the cells at coordinate (x,y)
Cell* getCellsNext(char** board, int size, int x, int y, int* nbCells){
    *nbCells = 0;
    Cell* cells = (Cell*)malloc((*nbCells) * sizeof(Cell));
    if (board[y][x-1] != '#' && board[y][x-1] != 'd'){ // left
        (*nbCells)++;
        cells = realloc(cells, (*nbCells) * sizeof(Cell));
        cells[*nbCells-1].x = x-1;
        cells[*nbCells-1].y = y;
    }
    if (board[y+1][x] != '#' && board[y+1][x] != 'g'){ // down
        (*nbCells)++;
        cells = realloc(cells, (*nbCells) * sizeof(Cell));
        cells[*nbCells - 1].x = x;
        cells[*nbCells - 1].y = y+1;
    }
    if (board[y][x+1] != '#' && board[y][x+1] != 'd'){ //right
        (*nbCells)++;
        cells = realloc(cells, (*nbCells) * sizeof(Cell));
        cells[*nbCells - 1].x = x+1;
        cells[*nbCells - 1].y = y;
    }
    if (board[y-1][x] != '#'){ // up
        (*nbCells)++;
        cells = realloc(cells, (*nbCells) * sizeof(Cell));
        cells[*nbCells - 1].x = x;
        cells[*nbCells - 1].y = y-1;
    }
    return cells;
}

void print2DarrayVector(Vector *v, int size){
    for (int i=0; i<size; i++){
        printf("(%d,%d) ",v[i].x,v[i].y);
    }
    printf("\n");
}
/*
Copyright (c) <year>, <copyright holder>
All rights reserved.

This source code is licensed under the BSD-style license found in the
LICENSE file in the root directory of this source tree.
*/
#include <ncurses.h>
#include <stdlib.h>
#include <stdio.h>
#include <locale.h>
#include <time.h>
#include <stdbool.h>
#include "board.h"
#include "draw.h"
#include "movePlayer.h"
#include "functions.h"
#include "moveGhost.h"

int main(){

    // ------------------- Init everything -------------------
    setlocale(LC_ALL, "");
    initscr();
    nodelay(stdscr, 1);
    noecho();
    cbreak();
    curs_set(0);
    keypad(stdscr, TRUE);
    initColors();
    srandom(time(NULL));

    // Setup GameBoard
    int size;
    char** board = readAllFileLine("board.txt", &size);
    Vector* player = searchPlayer(board, size);
    Vector* vector = (Vector*)malloc(sizeof(Vector));
    Vector* ghosts = initGhost(board, size);
    Vector oldVector;
    vector->x = 1;
    vector->y = 0;

    // Define some random usefull var
    bool activePacGum = true;
    bool running = true;
    bool victory = false;
    int iteration = 0;
    int nbBoucles = 0;

    // ------------------- Main loop -------------------
    while (running){
        refresh();
        napms(200);
        oldVector.x = vector->x; oldVector.y = vector->y;

        // drawPacGum
        if (iteration == 3){
            if (activePacGum){activePacGum = false;}else{activePacGum = true;}
            iteration = 0;
        } else {iteration++;}
        drawBoard(board, size, activePacGum);

        // get user input
        int input = getch();
        if (input != ERR){
            switch (input){
            case KEY_DOWN:
                vector->y = 1; vector->x = 0;
                break;
            case KEY_UP:
                vector->y = -1; vector->x = 0;
                break;
            case KEY_RIGHT:
                vector->x = 1; vector->y = 0;
                break;
            case KEY_LEFT:
                vector->x = -1; vector->y = 0;
                break;
            
            // Exit
            case 27:
            case KEY_BACKSPACE:
                running = false;
                break;
            }
        }

        // move ghosts
        ghosts = moveAllGhost(board, size, 0, ghosts, *player);
        
        // move player
        int returnMove = movePlayer(board, size, player, vector);
        
        // if in a wall
        if (returnMove == 2){ // continue on same path
            vector->x = oldVector.x; vector->y = oldVector.y;
            returnMove = movePlayer(board, size, player, vector);
        }

        // ate pacgum
        if (returnMove == 1){

        }
        // in a ghost
        else if(returnMove == 3){
            running = false;
            victory = false;
        }
    }
    // ------------------- End of main loop -------------------

    if(victory){mvprintw(size+1,3,"Vous avez gagné !");}
    else {mvprintw(size+1,3,"Vous avez perdu !");}
    running = true;

    // ------------------- Victory/Lose screen -------------------
    while (running){
        refresh();
        
        // attendre que le joueur quitte la partie
        int input = getch();
        if (input != ERR){
            switch (input){
            case 27:
            case KEY_BACKSPACE:
                running = 0;
                break;
            }
        }
    }

    // ------------------- Close all -------------------
    endwin();
    freeArray(board,size);
    free(ghosts);
    free(player);
    free(vector);    
    return 0;
}

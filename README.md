# Pacman in C with ncurses

## Description

This is a pacman game written in C with the ncurses lib (in a terminal).

## How to play

To play the game you just have to write this command in this folder :
```
make play
```
This command will automatically compile every files, launch the game, and clean the repertory after playing.

## In game

There is 6 input to know to play the game
- [ ] The 4 arrow to control pacman (up,down,left,right)
- [ ] The DELETE or ESCAPE key to stop the game

And that's it ! enjoy :)

## More
Every MAKE commands :
- [ ] make : to clean and re-compile every files
- [ ] make play : to compile, play and clean after the game
- [ ] make clean : to erase every .o files and the executable
- [ ] make debug : to debug the project with Valgrind
- [ ] make run : to compile the files without erasing the files at first

## License
This open source project is under BSD-3 license.

## Project status
In progress

FILES = main.o draw.o board.o movePlayer.o moveGhost.o functions.o
EXECFILE = main
LIB = -lncursesw -lm

.PHONY: all clean run debug

all: clean run

debug: $(FILES)
	gcc -g -o $(EXECFILE) $(FILES) $(LIB)

play: all
	./$(EXECFILE)
	rm -f $(EXECFILE) *.o

run: $(FILES)
	gcc -o $(EXECFILE) $(FILES) $(LIB)

board.o: board.c
draw.o: draw.c
functions.o: functions.c
main.o: main.c
moveGhost.o: moveGhost.c
movePlayer.o: movePlayer.c

%.o:
	gcc -c $< -Iinclude

clean:
	rm -f $(EXECFILE) *.o
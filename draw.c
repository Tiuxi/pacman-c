/*
Copyright (c) <year>, <copyright holder>
All rights reserved.

This source code is licensed under the BSD-style license found in the
LICENSE file in the root directory of this source tree.
*/
#include "draw.h"

void initColors(){
    start_color();

    // init custom color to overwrite unused color
    init_color(COLOR_CYAN,0,1000,1000);
    init_color(COLOR_YELLOW,1000,1000,0);
    init_color(COLOR_MAGENTA,1000,0,1000);

    // init color pairs
    init_pair(PAIR_WALL, COLOR_BLACK, COLOR_BLUE);
    init_pair(PAIR_BASE, COLOR_WHITE, COLOR_BLACK);
    init_pair(PAIR_PACMAN, COLOR_YELLOW, COLOR_BLACK);
    init_pair(PAIR_CAGE, COLOR_BLUE, COLOR_BLACK);
    
    init_pair(PAIR_GHOST1, COLOR_RED, COLOR_BLACK);
    init_pair(PAIR_GHOST2, COLOR_CYAN, COLOR_BLACK);
    init_pair(PAIR_GHOST3, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(PAIR_GHOST4, COLOR_YELLOW, COLOR_BLACK);

}

void clearCell(int x, int y){
    attron(COLOR_PAIR(PAIR_BASE));
    mvprintw(y,x*2,"  ");
    attroff(COLOR_PAIR(PAIR_BASE));
}

void drawWall(int x, int y){
    attron(COLOR_PAIR(PAIR_WALL));

    mvprintw(y,x*2,"  ");

    attroff(COLOR_PAIR(PAIR_WALL));
}

void drawGhost(int x, int y, char ghost, int color){
    if (ghost == '1'){ // RED
        attron(COLOR_PAIR(PAIR_GHOST1));
        mvprintw(y,x*2,"\u15E3 ");
        attroff(COLOR_PAIR(PAIR_GHOST1));
    }else if (ghost == '2'){ // CYAN
        attron(COLOR_PAIR(PAIR_GHOST2));
        mvprintw(y,x*2,"\u15E3 ");
        attroff(COLOR_PAIR(PAIR_GHOST2));
    }else if (ghost == '3'){ // PINK
        attron(COLOR_PAIR(PAIR_GHOST3));
        mvprintw(y,x*2,"\u15E3 ");
        attroff(COLOR_PAIR(PAIR_GHOST3));
    }else if (ghost == '4'){ // YELLOW
        attron(COLOR_PAIR(PAIR_GHOST4));
        mvprintw(y,x*2,"\u15E3 ");
        attroff(COLOR_PAIR(PAIR_GHOST4));
    }
}

void drawPacGum(int x, int y){
    mvprintw(y,x*2,"\u23FA");
}

void drawPac(int x, int y){
    mvprintw(y, x*2, "\u00B7 ");
}

void drawPlayer(int x, int y){
    attron(COLOR_PAIR(PAIR_PACMAN));
    mvprintw(y, x * 2, "\u2B24");
    attroff(COLOR_PAIR(PAIR_PACMAN));
}

void drawCageEntrance(int x, int y){
    attron(COLOR_PAIR(PAIR_CAGE));
    mvprintw(y, x * 2, "==");
    attroff(COLOR_PAIR(PAIR_CAGE));
}

void drawBoard(char** board, int size, bool activePacGum){
    for (int y=0; y<size; y++){
        for (int x=0; board[y][x]!='\0'; x++){
            switch (board[y][x]){
            // nothing
            case ' ':
            case 'd':
                clearCell(x,y);
                break;
            
            // wall
            case '#':
                drawWall(x,y);
                break;
            
            // pac
            case '.':
                drawPac(x,y);
                break;
            
            // pacgum
            case 'o':
                if (activePacGum) {drawPacGum(x,y);} else {clearCell(x,y);}
                break;
            
            // ghost cage
            case 'g':
                drawCageEntrance(x,y);
            
            // ghost
            case '1':
                drawGhost(x,y,board[y][x],PAIR_GHOST1);
                break;
            case '2':
                drawGhost(x,y,board[y][x],PAIR_GHOST2);
                break;
            case '3':
                drawGhost(x,y,board[y][x],PAIR_GHOST3);
                break;
            case '4':
                drawGhost(x,y,board[y][x],PAIR_GHOST4);
                break;
            
            // pacman
            case 'p':
            case 'n':
                drawPlayer(x,y);
                break;
            }
        }
    }
}
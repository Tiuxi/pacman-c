#ifndef FUNCTIONS
#define FUNCTIONS

#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <stdbool.h>
#include <ncurses.h>
#include <stdio.h>

typedef struct structCell{
    int x;
    int y;
}Vector;

typedef Vector Ghost;
typedef Vector Cell;

double dist(Vector, Vector);
Cell* getCellsNext(char ** board, int size, int x, int y, int* nbCells);
int randRange(int, int);

void freeVector(Vector);
bool equalsVector(Vector, Vector);
Vector* popVectorList(Vector* v, int size, int indexPop);

int closest(Vector p, Vector* v, int size);
Vector searchChar(char** board, int size, char c);
void print2DarrayVector(Vector* v, int size);
#endif
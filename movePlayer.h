#ifndef MOVEPLAYER
#define MOVEPLAYER

#include <stdlib.h>
#include <stdbool.h>
#include "functions.h"

Vector* searchPlayer(char**, int);
int movePlayer(char**, int, Vector*, Vector*);


#endif
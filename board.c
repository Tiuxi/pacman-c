/*
Copyright (c) <year>, <copyright holder>
All rights reserved.

This source code is licensed under the BSD-style license found in the
LICENSE file in the root directory of this source tree.
*/
#include "board.h"
/*
    Board :
' ' : space
'd' : door
'#' : wall
'.' : pac
'o' : pacgum
'g' : ghost cage door

    Ghost (precision in moveGhost.c):
'1', 'a', 'w' : blinky (red)
'2', 'z', 'x' : inky (cyan)
'3', 'e', 'c' : pinky (pink)
'4', 'r', 'v' : clyde (orange)

    Player :
'p' : player
'n' : player in a door
*/
char** readAllFileLine(char* level ,int* size){
    FILE* file = fopen(level, "r");

    char** result = (char**)malloc(sizeof(char*));
    int i=0;

    char line[MAX_CHAR_PER_LINE];

    while (fgets(line, sizeof(line), file)) {
        result = realloc(result, (i+1) * sizeof(char*));
        result[i] = (char*)malloc(MAX_CHAR_PER_LINE * sizeof(char));
        for (int j = 0; j<MAX_CHAR_PER_LINE; j++) {
            result[i][j] = line[j];
        }
        result[i][MAX_CHAR_PER_LINE-1] = '\0';
        i++;
    }

    fclose(file);
    *size = i;
    return result;
}

void freeArray(char** array, int size){
    for (int i=0; i<size; i++){
        free(array[i]);
    }
    free(array);
}
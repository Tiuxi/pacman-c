/*
Copyright (c) <year>, <copyright holder>
All rights reserved.

This source code is licensed under the BSD-style license found in the
LICENSE file in the root directory of this source tree.
*/
#include "moveGhost.h"

Vector* initGhost(char** board, int size){
    Vector* ghosts = (Vector*)malloc(8 * sizeof(Vector));
    
    // blinky (red)
    Vector ghostCoord = searchChar(board, size, '1');
    ghosts[0] = ghostCoord; ghosts[4] = ghostCoord;

    // inky (cyan)
    ghostCoord = searchChar(board, size, '2');
    ghosts[1] = ghostCoord; ghosts[5] = ghostCoord;

    // pinky (pink)
    ghostCoord = searchChar(board, size, '3');
    ghosts[2] = ghostCoord; ghosts[6] = ghostCoord;

    // clyde (orange)
    ghostCoord = searchChar(board, size, '4');
    ghosts[3] = ghostCoord; ghosts[7] = ghostCoord;

    return ghosts;
}

// red
Vector blinky(char** board, int size, Vector coord, Vector oldCoord, Vector target){
    Vector result;

    // get numbers of cell adjacent to current location of the ghost
    int nbCells;
    Cell* cellsNext = getCellsNext(board, size, coord.x, coord.y, &nbCells);

    // ------------------- In a corrider (2 cells possibility) -------------------
    /*
        1% turn back (if target behind)
        99-100% continue on the same direction
    */
    if (nbCells == 2){

        // determine closest cell next to blinky
        int indexClosest = closest(target, cellsNext, 2);

        // 1% chance of turning back if target is behind
        if (equalsVector(cellsNext[indexClosest], oldCoord)){
            int random = randRange(1,100);

            // blinky turn back
            if (random == 1){
                result.x = cellsNext[indexClosest].x;
                result.y = cellsNext[indexClosest].y;
            }else {
                result.x = cellsNext[(indexClosest + 1) % 2].x;
                result.y = cellsNext[(indexClosest + 1) % 2].y;
            }
        }

        // target is in front
        else {
            result.x = cellsNext[indexClosest].x;
            result.y = cellsNext[indexClosest].y;
        }
    }

    // ------------------- In a Intersection (3 cells possibility) -------------------
    /*
        0-1% turn back (if target behind)
        11-12% take 2nd closest path to target
        87% take closest path to target
    */
    else if (nbCells == 3){
        int indexClosest = closest(target, cellsNext, 3);

        int random = randRange(1,100);

        // target is behind
        if (equalsVector(cellsNext[indexClosest], oldCoord)){

            // Blinky turn back
            if (random == 1){
                result.x = cellsNext[indexClosest].x;
                result.y = cellsNext[indexClosest].y;
            }else {
                cellsNext = popVectorList(cellsNext, 3, indexClosest);
                int indexClosest = closest(target, cellsNext, 2);

                // take 2nd closest cell from target that is not his previous location
                if (random <= 12){
                    result.x = cellsNext[(indexClosest + 1) % 2].x;
                    result.y = cellsNext[(indexClosest + 1) % 2].y;
                }
                // take closest cell from target that is not his previous location
                else {
                    result.x = cellsNext[indexClosest].x;
                    result.y = cellsNext[indexClosest].y;
                }
            }
        }

        // target isn't behind
        else {
            // take 2nd closest cell from target
            if (random <= 12){

                cellsNext = popVectorList(cellsNext, 3, indexClosest);
                int indexClosest = closest(target, cellsNext, 2);

                // target is 2nd closest
                if (equalsVector(cellsNext[indexClosest],oldCoord)) {
                    //turn back
                    if (random == 1){
                        result.x = cellsNext[indexClosest].x;
                        result.y = cellsNext[indexClosest].y;
                    }
                    // don't turn back
                    else{
                        result.x = cellsNext[(indexClosest + 1) % 2].x;
                        result.y = cellsNext[(indexClosest + 1) % 2].y;
                    }
                }
                // target not behind
                else{
                    // can't turn back
                    result.x = cellsNext[indexClosest].x;
                    result.y = cellsNext[indexClosest].y;
                }

            }
            // take closest cell from target
            else{
                result.x = cellsNext[indexClosest].x;
                result.y = cellsNext[indexClosest].y;
            }
        }
    }

    // ------------------- In a Intersection (4 cells possibility) -------------------
    /*
        0-1% turn back (if target behind)
        7-8% take 3rd closest path to target
        12% take 2nd closest path to target
        80% take closest path to target
    */
    else if (nbCells == 4){
        int indexClosest = closest(target, cellsNext, 4);

        int random = randRange(1,100);

        // target is behind
        if (equalsVector(cellsNext[indexClosest], oldCoord)){

            // Blinky turn back
            if (random == 1){
                result.x = cellsNext[indexClosest].x;
                result.y = cellsNext[indexClosest].y;
            }else {
                cellsNext = popVectorList(cellsNext, 4, indexClosest);
                int indexClosest = closest(target, cellsNext, 3);

                // don't take closest cell
                if (random <= 20){              // 8% + 12 %
                    cellsNext = popVectorList(cellsNext, 3, indexClosest);
                    int indexClosest = closest(target, cellsNext, 2);

                    // take 3rd closest cell
                    if (random <= 8){
                        result.x = cellsNext[indexClosest].x;
                        result.y = cellsNext[indexClosest].y;
                    }
                    // take 2nd closest cell
                    else {
                        result.x = cellsNext[indexClosest].x;
                        result.y = cellsNext[indexClosest].y;
                    }
                }
                // take closest cell
                else {
                    result.x = cellsNext[indexClosest].x;
                    result.y = cellsNext[indexClosest].y;
                }
            }
        }

        // target isn't behind
        else {
            // don't take closest cell
            if (random <= 20){          // 8% + 12%

                cellsNext = popVectorList(cellsNext, 4, indexClosest);
                int indexClosest = closest(target, cellsNext, 3);

                // 2nd closest is previous location
                if (equalsVector(cellsNext[indexClosest], oldCoord)){
                    // pop previous location
                    cellsNext = popVectorList(cellsNext, 3, indexClosest);
                    int indexClosest = closest(target, cellsNext, 2);

                    // take 3rd closest cell
                    if (random <= 15){
                        result.x = cellsNext[(indexClosest + 1) % 2].x;
                        result.y = cellsNext[(indexClosest + 1) % 2].y;
                    }
                    // take 2nd closest cell
                    else {
                        result.x = cellsNext[indexClosest].x;
                        result.y = cellsNext[indexClosest].y;
                    }
                } 
                // 2nd closest cell isn't previous location
                else{
                    // take 3rd closest cell 
                    if (random <= 8) {
                        // pop 2nd closest cell
                        cellsNext = popVectorList(cellsNext, 3, indexClosest);
                        int indexClosest = closest(target, cellsNext, 2);

                        // 3rd closest cell is previous location
                        if (equalsVector(cellsNext[indexClosest], oldCoord)){
                            result.x = cellsNext[(indexClosest + 1) % 2].x;
                            result.y = cellsNext[(indexClosest + 1) % 2].y;
                        }
                        // 3rd closest cell isn't previous location
                        else {
                            result.x = cellsNext[indexClosest].x;
                            result.y = cellsNext[indexClosest].y;
                        }
                    }
                    // take 2nd closest cell
                    else {
                        result.x = cellsNext[indexClosest].x;
                        result.y = cellsNext[indexClosest].y;
                    }
                }

            }
            // take closest cell
            else{
                result.x = cellsNext[indexClosest].x;
                result.y = cellsNext[indexClosest].y;
            }
        }
    }
    free(cellsNext);
    return result;
}

// cyan
void inky(){}

// pink
void pinky(){}

// orange
void clyde(){}

/*
Blinky (red) : 
    1 : ghost
    a : ghost + pac
    w : ghost + pacgum

Inky (cyan) :
    2 : ghost
    z : ghost + pac
    x : ghost + pacgum

Pinky (pink) :
    3 : ghost
    e : ghost + pac
    c : ghost + pacgum
*/

Vector* moveAllGhost(char** board, int size, int nboucle, Vector* ghosts, Vector player){
    Vector oldCoord;

    // blinky
    oldCoord = ghosts[0];
    ghosts[0] = blinky(board, size, ghosts[0], ghosts[4], player);
    ghosts[4] = oldCoord;
    
    board[ghosts[0].y][ghosts[0].x] = '1';
    board[oldCoord.y][oldCoord.x] = ' ';

    return ghosts;
}
